﻿using System;
using System.Collections;

public interface ISceneLoader
{
    IEnumerator LoadScene(Action onSceneLoaded = null);
}