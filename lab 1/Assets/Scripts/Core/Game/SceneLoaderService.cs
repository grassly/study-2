﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoaderService : ISceneLoader
{
    public const string BOOTSTRAP_SCENE_NAME = "Main";

    public IEnumerator LoadScene(Action onSceneLoaded = null)
    {
        AsyncOperation sceneLoadOperation = SceneManager.LoadSceneAsync(BOOTSTRAP_SCENE_NAME);

        while (!sceneLoadOperation.isDone)
        {
            yield return null;
        }
        onSceneLoaded?.Invoke();
    }
}
