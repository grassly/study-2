using UnityEngine;
using Zenject;

public class ProjectInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<ISceneLoader>().To<SceneLoaderService>().AsSingle();
        Container.Bind<IAdService>().To<AdService>().AsSingle();
    }
}
