﻿using UnityEngine;
using Zenject;

public class GameBootstrapper : MonoBehaviour
{
    [Inject]
    private ISceneLoader _iSceneLoader; 

    [Inject]
    private IAdService _iAdService;

    private void Awake()
    {
        DontDestroyOnLoad(this);

        _iAdService.InitAd();

        StartCoroutine(_iSceneLoader.LoadScene(() =>
        {
            StartCoroutine(_iAdService.ShowAd(() =>
                { Debug.Log("Works!"); }));
        }));
    }
}
