﻿using System;
using System.Collections;

public interface IAdService
{
    void InitAd();
    IEnumerator ShowAd(Action onAdsShow = null);
}