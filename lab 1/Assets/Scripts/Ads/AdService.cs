﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdService : IAdService, IUnityAdsInitializationListener, IUnityAdsLoadListener, IUnityAdsShowListener
{
    private const string ANDROID_GAME_ID = "4300159";
    private const string IOS_GAME_ID = "4300158";
    private string _gameId;

    private const string ANDROID_AD_UNIT_ID = "Interstitial_Android";
    private const string IOS_AD_UNIT_ID = "Interstitial_iOS";
    private string _adUnitId;

    public void InitAd()
    {
        #region Set game id
        _gameId = (Application.platform == RuntimePlatform.IPhonePlayer)
            ? IOS_GAME_ID
            : ANDROID_GAME_ID;
        Advertisement.Initialize(_gameId);
        #endregion

        #region Set ads unit
        _adUnitId = (Application.platform == RuntimePlatform.IPhonePlayer)
            ? IOS_AD_UNIT_ID
            : ANDROID_AD_UNIT_ID;
        #endregion
    }

    public void LoadAd() =>
        Advertisement.Load(_adUnitId, this);

    public IEnumerator ShowAd(Action onAdsShow = null)
    {
        LoadAd();
        while (Advertisement.IsReady(_adUnitId))
        {
            yield return null;
        }
        Advertisement.Banner.SetPosition(BannerPosition.TOP_CENTER);
        Advertisement.Banner.Show(_adUnitId);

        onAdsShow?.Invoke();
    }

    public void OnInitializationComplete() =>
    Debug.Log("Ads initialization complete!");

    public void OnInitializationFailed(UnityAdsInitializationError error, string message) =>
        Debug.Log($"Ads Initialization Failed: {error.ToString()} - {message}");

    public void OnUnityAdsAdLoaded(string placementId) =>
        Debug.Log("Ads loaded!");

    public void OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message) =>
        Debug.Log($"Error loading Ad Unit: {_adUnitId} - {error.ToString()} - {message}");

    public void OnUnityAdsShowFailure(string placementId, UnityAdsShowError error, string message) =>
        Debug.Log($"Error showing Ad Unit {_adUnitId}: {error.ToString()} - {message}");

    public void OnUnityAdsShowStart(string placementId) { }
    public void OnUnityAdsShowClick(string placementId) { }
    public void OnUnityAdsShowComplete(string placementId, UnityAdsShowCompletionState showCompletionState) { }
}
