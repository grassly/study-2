using TMPro;
using UnityEngine;
using Zenject;

public class ProjectInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<IObjectPool>().To<ObjectPool>().AsSingle()/*.WithArguments(poolSize, itemPrefab, startPosition)*/;
        Container.Bind<IUISwitcher>().To<UISwitcher>().AsSingle()/*.WithArguments(scoreText, winText)*/;
        //Container.Bind<ISpawner>().To<Spawner>().AsSingle();
    }
}