using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelMovement : MonoBehaviour
{
    public float speed = 1f;

    private void MoveDown() => 
        transform.position -= Vector3.up * speed * Time.deltaTime;

    private void Update() => 
        MoveDown();
}
