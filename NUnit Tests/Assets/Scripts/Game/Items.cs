﻿public static class Bomb
{
    public static int LifeCost = -1;
    public static int ScoreCost = -10;
}

public static class Bubble
{
    public static int LifeCost = 0;
    public static int ScoreCost = 1;
}

public static class PoisonedBubble
{
    public static int LifeCost = -1;
    public static int ScoreCost = 1;
}