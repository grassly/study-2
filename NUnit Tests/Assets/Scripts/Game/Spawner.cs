using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Spawner : MonoBehaviour/*, ISpawner*/
{
    [Inject] private IObjectPool _pool;
    
    private float _minSpawnTime = .5f;
    private float _maxSpawnTime = 2f;

    private bool enableSpawn = true;

    public void StartSpawn() => 
        StartCoroutine(SpawnObjects());

    public IEnumerator SpawnObjects()
    {
        if (!enableSpawn)
            yield break;
        
        Spawn();

        yield return new WaitForSeconds(UnityEngine.Random.Range(_minSpawnTime, _maxSpawnTime));
        StartCoroutine(SpawnObjects());
    }

    private void Spawn()
    {
        PoolableItem obj = _pool.GetPoolObject();
        if (obj != null)
            obj.gameObject.SetActive(true);
    }

    public void StopSpawn() => 
        enableSpawn = false;
}