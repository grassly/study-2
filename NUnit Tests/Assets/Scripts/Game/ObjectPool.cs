﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ObjectPool : IObjectPool
{
    private Queue<PoolableItem> Pool = new Queue<PoolableItem>();

    private int _poolSize = 5;
    private GameObject _itemPrefab;
    private Vector3 _startPosition = new Vector3(0, 8, 0);
    //[Inject] private DiContainer _diContainer;

    public void Init (GameObject itemPrefab) => 
        _itemPrefab = itemPrefab;

    public void InstantiatePool()
    {
        for (int i = 0; i < _poolSize; i++)
        {
            //GameObject item = _diContainer.InstantiatePrefab(_itemPrefab).gameObject;
            GameObject item = MonoBehaviour.Instantiate(_itemPrefab);
            PoolableItem itemSettings = item.GetComponent<PoolableItem>();
;
            item.transform.position =
                new Vector3(UnityEngine.Random.Range(-11, 11), _startPosition.y, _startPosition.z);

            if (i == _poolSize - 1)
            {
                itemSettings.LifeCost = Bomb.LifeCost;
                itemSettings.ScoreCost = Bomb.ScoreCost;
                item.transform.localScale = new Vector3(3,3, 3);
            }
            else if (i < _poolSize / 2)
            {
                itemSettings.LifeCost = PoisonedBubble.LifeCost;
                itemSettings.ScoreCost = PoisonedBubble.ScoreCost;
                item.transform.localScale = new Vector3(2, 2, 2);
            }
            else
            {
                itemSettings.LifeCost = Bubble.LifeCost;
                itemSettings.ScoreCost = Bubble.ScoreCost;
                item.transform.localScale = new Vector3(1, 1, 1);
            }
                
            
            ReturnToPool(item.GetComponent<PoolableItem>());
        }
    }
    
    public PoolableItem GetPoolObject() => 
        Pool.Dequeue();

    public void ReturnToPool(PoolableItem item) =>
        Pool.Enqueue(item);
}
