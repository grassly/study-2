using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class Bootstrapper : MonoBehaviour
{
    //[Inject] private DiContainer _diContainer;

    [Inject] private IObjectPool _objectPool;
    [Inject] private IUISwitcher _uiSwitcher;

    [SerializeField] private Spawner spawner;
    [SerializeField] private GameObject itemPrefab;
    
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI hpText;
    [SerializeField] private GameObject winText;

    private Game _game;
    private void Awake()
    {
        //_game = Instantiate(new GameObject()).AddComponent<Game>();
        _game = new Game();
        _game.InitGame(0, 3);
        
        _objectPool.Init(itemPrefab);
        _objectPool.InstantiatePool();
        _uiSwitcher.Init(scoreText, hpText, winText);
        _uiSwitcher.RefreshScore(_game.GameScore.ToString());
        _uiSwitcher.RefreshLives(_game.Hp.ToString());
        spawner.StartSpawn();
        
        //_diContainer.InstantiatePrefab(Resources.Load<GameObject>("Spawner"));
        //_diContainer.InstantiatePrefab(Resources.Load<GameObject>("Game"));

        PoolableItem.OnTurnOff += _objectPool.ReturnToPool;
        PoolableItem.OnClickToScore += _game.AddScore;
        PoolableItem.OnClickToLife += _game.AddLives;
        Game.OnScoreValueChanged += _uiSwitcher.RefreshScore;
        Game.OnLivesValueChanged += _uiSwitcher.RefreshLives;
        Game.OnWin += spawner.StopSpawn;
        Game.OnWin +=_uiSwitcher.ShowWinMessage;
        Game.OnWin += FinishGame;
    }
    
    private void FinishGame()
    {
        PoolableItem.OnClickToScore -= _game.AddScore;
        PoolableItem.OnClickToLife -= _game.AddLives;
    }

    private void OnDisable()
    {
        PoolableItem.OnTurnOff -= _objectPool.ReturnToPool;
        PoolableItem.OnClickToScore -= _game.AddScore;
        PoolableItem.OnClickToLife -= _game.AddLives;
        Game.OnScoreValueChanged -= _uiSwitcher.RefreshScore;
        Game.OnLivesValueChanged += _uiSwitcher.RefreshLives;
        Game.OnWin -= spawner.StopSpawn;
        Game.OnWin -=_uiSwitcher.ShowWinMessage;
        Game.OnWin -= FinishGame;
    }
}
