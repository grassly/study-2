using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObjectPool
{
    void Init (GameObject itemPrefab);
    void InstantiatePool();
    PoolableItem GetPoolObject();
    void ReturnToPool(PoolableItem item);
}