﻿using System.Collections;

public interface ISpawner
{
    IEnumerator SpawnObjects();
    void StopSpawn();
    void StartSpawn();
}