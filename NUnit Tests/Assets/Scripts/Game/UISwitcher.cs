﻿using UnityEngine.UI;
using TMPro;
using UnityEngine;
using Zenject;

public class UISwitcher : IUISwitcher
{
    private TextMeshProUGUI _scoreText;
    private TextMeshProUGUI _hpText;
    private GameObject _winText;
    
    public void Init(TextMeshProUGUI scoreText, TextMeshProUGUI hpText, GameObject winText)
    {
        _scoreText = scoreText;
        _hpText = hpText;
        _winText = winText;
    }

    public void RefreshScore(string score) => 
        _scoreText.text = $"Score: {score}";
    
    public void RefreshLives(string lives) => 
        _hpText.text = $"Lives: {lives}";

    public void ShowWinMessage() => 
        _winText.SetActive(true);
}