using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPoolableItem
{
    delegate void OnCoinPickUp();
    event OnCoinPickUp OnCoinPickedUp;
    int LifeCost { get; set; }
    int ScoreCost { get; set; }
}