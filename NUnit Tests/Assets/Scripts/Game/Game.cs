using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class Game 
{
    public static Action<string> OnScoreValueChanged;
    public static Action<string> OnLivesValueChanged;
    public static Action OnWin;
    private int _score;
    public int GameScore
    {
        get => _score;
        set
        {
            _score = value;
            if (_score == 10)
                Win();
        }
    }

    private int _hp;
    public int Hp    
    {
        get => _hp;
        set
        {
            _hp = value;
            if (_hp <= 0)
                Lose();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
            ReloadScene();
    }

    public void InitGame(int score, int hp)
    {
        _score = score;
        _hp = hp;
    }

    public void AddScore(int scoreCost)
    {
        GameScore += scoreCost;
        OnScoreValueChanged?.Invoke(GameScore.ToString());
    }

    public void AddLives(int lifeCost)
    {
        Hp += lifeCost;
        OnLivesValueChanged?.Invoke(Hp.ToString());
    }

    private void Win() => 
        OnWin?.Invoke();

    private void Lose() =>
        ReloadScene();
    
    private void ReloadScene() =>
        SceneManager.LoadScene(0);
}
