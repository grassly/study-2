﻿using System;
using UnityEngine;

public class PoolableItem : MonoBehaviour, IPoolableItem
{
    public static Action<PoolableItem> OnTurnOff;
    public static Action<int> OnClickToScore;
    public static Action<int> OnClickToLife;
    
    private float yPosition = 8;

    public event IPoolableItem.OnCoinPickUp OnCoinPickedUp;
    public int LifeCost { get; set; }
    public int ScoreCost { get; set; }

    public Material mat;

    private void OnMouseDown()
    {
        OnClickToScore?.Invoke(ScoreCost);
        OnClickToLife?.Invoke(LifeCost);
        TurnOffObject();
    }

    private void OnBecameInvisible() => 
        TurnOffObject();

    private void TurnOffObject()
    {
        gameObject.SetActive(false);
        Vector3 currPosition = transform.position;
        gameObject.transform.position = new Vector3(currPosition.x, yPosition, currPosition.z);

        OnTurnOff?.Invoke(this);
    }
}

