using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public interface IUISwitcher
{
    void Init(TextMeshProUGUI scoreText, TextMeshProUGUI hpText, GameObject winText);
    void RefreshScore(string score);
    void RefreshLives(string lives);
    void ShowWinMessage();
}