using System.Collections;
using System.Collections.Generic;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class GameTests
    {
        [SetUp]
        public void SetUp()
        {
            
        }
        
        [Test]
        public void PerformClick()
        {
            //Arrange
            //Game game = GameObject.Instantiate(new GameObject()).AddComponent<Game>();

            var mock = new Mock<IPoolableItem>();
            mock.Setup(c => c.ScoreCost).Returns(1);
            mock.Setup(c => c.LifeCost).Returns(-1);
            mock.Raise(e => e.OnCoinPickedUp += () =>
            {
                
            });
            //Act
            //mock.Setup(c => c).Invoking();
            //Assert
        }


        [Test]
        public void AddScore()
        {
            //Arrange
            //Game game = GameObject.Instantiate(new GameObject()).AddComponent<Game>();
            Game game = new Game();
            //Act
            game.AddScore(1); 
            //Assert
            game.GameScore.Should().Be(1);
        }
    }
}
