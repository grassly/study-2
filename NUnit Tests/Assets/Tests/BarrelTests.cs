using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class BarrelTests
    {
        private BarrelMovement _barrelMovement;
        private float _testTime = .1f;
        
        [SetUp]
        public void SetUp()
        {
            GameObject gameObject = 
                MonoBehaviour.Instantiate(Resources.Load<GameObject>("Barrel"));
            _barrelMovement = gameObject.GetComponent<BarrelMovement>();
        }
        
        [UnityTest]
        public IEnumerator TestBarrelMovement()
        {
            yield return new WaitForSeconds(_testTime + .1f);
            
            Assert.Less(_barrelMovement.transform.position.y, _testTime * _barrelMovement.speed);
        }

        [TearDown]
        public void TearDown()
        {
            Object.Destroy(_barrelMovement.gameObject);
        }
    }
}
